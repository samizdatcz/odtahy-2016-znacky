container = d3.select ig.containers.base
mapElement = container.append \div
  ..attr \class \map
bounds =
  x: [14.263 14.689]
  y: [49.952 50.171]
center = [(bounds.y.0 + bounds.y.1) / 2, (bounds.x.0 + bounds.x.1) / 2]
center.0 += 0.023
center.1 -= 0.04
zoom = 12
if ig.isRychlost
  zoom = 12
  center = [50.06199 14.4070]
maxBounds = [[49.94,14.24], [50.18,14.7]]
map = L.map do
  * mapElement.node!
  * minZoom: 12,
    maxZoom: 18,
    zoom: zoom,
    center: center
    maxBounds: maxBounds

baseLayer = L.tileLayer do
  * "https://samizdat.cz/tiles/ton_b1/{z}/{x}/{y}.png"
  * zIndex: 1
    opacity: 1
    attribution: 'mapová data &copy; přispěvatelé <a target="_blank" href="http://osm.org">OpenStreetMap</a>, obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>'

labelLayer = L.tileLayer do
  * "https://samizdat.cz/tiles/ton_l1/{z}/{x}/{y}.png"
  * zIndex: 3
    opacity: 0.75

map.addLayer baseLayer
map.addLayer labelLayer
topo = ig.data.data
geojson = topojson.feature topo, topo.objects."data"
layer = L.geoJson do
  * geojson

map.addLayer layer
setZnacka = (znackaId) ->
  values = geojson.features.map (feature) ->
    feature.properties[znackaId] / feature.properties.total
  values.sort (a, b) -> a - b

  scale = d3.scale.quantile!
    ..domain values
    ..range ['#ffffcc','#ffeda0','#fed976','#feb24c','#fd8d3c','#fc4e2a','#e31a1c','#bd0026','#800026']
  layer.setStyle (feature) ->
    weight: 1
    opacity: 0.8
    fillOpacity: 0.5
    color: scale feature.properties[znackaId] / feature.properties.total

class Znacka
  (@id, @human) ->

znacky =
  new Znacka \skoda "Škoda"
  new Znacka \ford "Ford"
  new Znacka \volkswagen "Volkswagen"
  new Znacka \mercedes "Mercedes"
  new Znacka \audi "Audi"
  new Znacka \bmw "BMW"

setZnacka znacky.0.id

container.append \select
  ..selectAll \option .data znacky .enter!append \option
    ..attr \value (.id)
    ..html (.human)
  ..on \change -> setZnacka @value


legend = new ig.Legend container, geojson.features

colors = [['#ffffcc','#ffeda0','#fed976','#feb24c','#fd8d3c','#fc4e2a','#e31a1c','#bd0026','#800026']]
  # ['rgb(252,251,253)','rgb(239,237,245)','rgb(218,218,235)','rgb(188,189,220)','rgb(158,154,200)','rgb(128,125,186)','rgb(106,81,163)','rgb(84,39,143)','rgb(63,0,125)']

assignments = [1 0 1 2 3 3 4 5 6 6 7 8]
class ig.Legend
  (@baseElement, features) ->
    features = features.filter ->
      it.properties.gid in [802 803 837 838 870 871 905 906 938 939 973 974]
    @element = @baseElement.append \div
      ..attr \class \legend
    width = 80
    {width, height, projection} = ig.utils.geo.getFittingProjection features, width

    path = d3.geo.path!
      ..projection projection
    @svgs = @element.selectAll \div .data colors .enter!append \div
      ..attr \class \line
      ..append \div
        .html (d, i) ->
          if i == 0 then "Menší<br>podíl" else "Méně<br>případů"
      ..append \svg
        ..attr \width width
        ..attr \height height
        ..selectAll \path .data features .enter!append \path
          ..attr \d path
          ..attr \fill (d, i, ii) ~> colors[ii][assignments[i]]

      ..append \div
        .html (d, i) ->
          if i == 0 then "Větší<br>podíl" else "Více<br>případů"

  setCount: ->
    @element.classed \count it
